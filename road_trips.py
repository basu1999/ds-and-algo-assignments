import sys
sys.setrecursionlimit(10**8)

Num_cities,Num_roads,Num_trips=[int(x) for x in input().split(' ')]
#print("cities= ",Num_cities,"roads= ",Num_roads,"Trips= ",Num_trips)

#Set of vertices
Vertices=list(range(1,Num_cities+1))
#Read the edges
Edges=[]
for i in range(0,Num_roads):
    u,v=[int(x) for x in input().split(' ')]
    #print("(u,v)=",u,v)
    edge=(u,v)
    Edges.append(edge)

#Read museum in cities
#museums=[int(x) for x in input().split(' ')]
Line=input()
values=Line.split(" ")
museums=[]
for x in range(0,Num_cities):
    val=values[x]
    museums.append(int(val))
#print("Museums= ",museums)

#print("Vertices= ",Vertices)
#print("Edges= ", Edges)
#print("Museums= ",museums)
#Adjacency dict
adj_dict={}
for v in Vertices:
        adj_dict.setdefault(v,[])
for edge in Edges:
    u,v=edge
    adj_dict[u].append(v)
    adj_dict[v].append(u)

#print("Adjacency dict= ", adj_dict)



#Graph Traversal
G=(Vertices,Edges)
num_components=0
color=[0]*len(Vertices)

sum_comp=[]
comp_total=0
def ADJ(v):
    return adj_dict[v]
def DFS(G):
    V=G[0]
    E=G[1]
    global num_components
    global color
    global comp_total
    for vertex in V:
        if (color[vertex-1]==0):
            num_components+=1
            comp_total=0
            DFS_VISIT(vertex)
            sum_comp.append(comp_total)
#Use DFS visit to find component sums
def DFS_VISIT(vertex):
    global num_components
    global color
    global comp_total
    color[vertex-1]=1
    comp_total+=museums[vertex-1]
    for neighbour in ADJ(vertex):
        if (color[neighbour-1]==0):
            DFS_VISIT(neighbour)
DFS(G)

#print("Num of components= ", num_components)
#print("Component: ",component)

#Number of museums
if (num_components <Num_trips):
    x=-1
    print(x)
else:
    sum_comp=sorted(sum_comp,reverse=True)
    total=0
    for i in range(0,Num_trips):
        if (i %2 ==0):
            x=sum_comp[0]
            sum_comp.remove(x)
        else:
            x=sum_comp[len(sum_comp)-1]
            sum_comp.remove(x)
        total+=x
    print(total)
        


