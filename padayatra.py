import sys
import queue
import heapq
sys.setrecursionlimit(10**8)
num_edges=int(input())

Edges=[]
actual_vertices=[]
weight_list=dict()
for i in range(0,num_edges):
    u,v,w=[int(x) for x in input().split(" ")]
    edge=(u,v)
    Edges.append(edge)
    if u not in actual_vertices:
        actual_vertices.append(u)
    if v not in actual_vertices:
        actual_vertices.append(v)
    weight_list[(u,v)]=w
    weight_list[(v,u)]=w

max_vertex=max(actual_vertices)
adjacency_list=dict()
for i in actual_vertices:
    adjacency_list.setdefault(i,[])
for edge in Edges:
    u,v=edge
    adjacency_list[u].append(v)
    adjacency_list[v].append(u)
    
    
    
    

#Find cycles
used_edges=[]
remaining_edges=Edges.copy()
def adj(u):
    return adjacency_list[u]
color=[0]*(max_vertex+1)
parent=[0]*(max_vertex+1)
def BFS(V):
    start=V[0]
    Q=queue.Queue()
    Q.put(start)
    color[start]=1
    while (not Q.empty()):
        vertex=Q.get()
        for neighbour in adj(vertex):
            if(color[neighbour]==0):
                Q.put(neighbour)
                color[neighbour]=1
                parent[neighbour]=vertex
                used_edges.append((vertex,neighbour))
                u=vertex
                v=neighbour
                if (u,v) in remaining_edges:
                    remaining_edges.remove((u,v))
                if (v,u) in remaining_edges:
                    remaining_edges.remove((v,u))
BFS(actual_vertices)

#Shortest Path and cost
def dijkstra(src,dest):
    dist=[sys.maxsize]*(max_vertex+1)
    path_shortest=[0]*(max_vertex+1)
    dist[src]=0
    VQ=[(dist[vertex],vertex) for vertex in actual_vertices]
    heapq.heapify(VQ)
    #VQ=actual_vertices.copy()

    sd_vertex=-1
    while(len(VQ)):
        sd_vertex=heapq.heappop(VQ)
        sd_vertex=sd_vertex[1]
        path_shortest[sd_vertex]=1
        for neighbour in adj(sd_vertex):
            if (path_shortest[neighbour]==0):
                new_distance=dist[sd_vertex]+weight_list[(sd_vertex,neighbour)]
                if(dist[neighbour]>new_distance):
                    VQ.remove((dist[neighbour],neighbour))
                    dist[neighbour]=new_distance
                    heapq.heappush(VQ,(dist[neighbour],neighbour))


    return dist[dest]



cost_list=[]
for edge in remaining_edges:
    src=edge[0]
    dest=edge[1]
    adjacency_list[src].remove(dest)
    adjacency_list[dest].remove(src)
    cost=dijkstra(src,dest)
    final_cost=cost+weight_list[(src,dest)]
    cost_list.append(final_cost)
    adjacency_list[src].append(dest)
    adjacency_list[dest].append(src)

#Answer    
cost_list.sort()
print(int(cost_list[0]))

    


