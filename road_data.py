import random
vertices=5000
edges=40000
comp=2
values=500
with open("data.txt","w") as f:
    f.write(f"{vertices} {edges} {comp}\n")
    for i in range(0,edges):
        x=random.randint(1,vertices)
        y=random.randint(1,vertices)
        while (x==y):
            y=random.randint(1,vertices)
        f.write(f"{x} {y}\n")
    val=""
    for i in range(0,vertices):
        x=random.randint(0,values)
        val+=str(x)
        val+=" "
    f.write(val)
